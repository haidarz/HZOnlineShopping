<?php
session_start();
require_once('functions/database.php');
$link=connect();
?>
<html>
<head>
<title>All markets</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="css/main.css"/>
<link rel="stylesheet" href="jquerymobile/jquery.mobile-1.4.5.min.css" />
<script src="jquerymobile/jquery.js"></script>
<script src="jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<!--Javascript-->
<script>
$(document).ready(function(){
	$('.gohome').click(function(){ 
			 window.location='./mainjq.php';
		 });
});	
</script>
<body>
	<div data-role='page' id='page_view_all_markets'>
		<div data-role='header' data-position='fixed'>
			<h1>All Markets</h1>
			<div data-role='navbar' data-iconpos='left'>
				<ul><li><a href='#' data-icon='home' class='gohome'>Home</a></li></ul>
					</div>
			</div>
			<div data-role='main' class='ui-content'>
<?php
		$query1="SELECT mid,address,lid FROM location;";
		$result1=mysqli_query($link,$query1);
		while($tab1=mysqli_fetch_row($result1)){
		$query1_1="SELECT name FROM markets where mid=$tab1[0];";
		$result1_1=mysqli_query($link,$query1_1);
		$tab1_1=mysqli_fetch_row($result1_1);
		echo "<div class='card' id= ><h2>$tab1_1[0]</h2><h3>$tab1[1]</h3></div>";
		}
		?>
		</div><!--End of main-->
	</div><!--End of page-->
		</body>
		</html>
		
