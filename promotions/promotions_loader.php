<?php
	session_start();
	require_once('../functions/database.php');
	$link=connect();
	if(isset($_GET['mid']) && isset($_GET['catid'])){
		extract($_GET);
		$products=array();
		$query1="SELECT name FROM markets WHERE mid=$mid";
		$res1=mysqli_query($link,$query1);
		$tab1=mysqli_fetch_row($res1);
		$market_name=$tab1[0];
		if($catid==0){
			$query2="SELECT * FROM promotions WHERE mid=$mid;";//Select all
		}else{
			$query2="SELECT * FROM promotions WHERE mid=$mid AND catid=$catid";//Select specific cat
		}		
		$res2=mysqli_query($link,$query2);
		//Case no product found
		if(mysqli_num_rows($res2)==0){
			header('location:../mainjq.php');
			exit;
		}else{
			//fill the products in an array
			$tmp=array();
			while($tmp=mysqli_fetch_row($res2)){
				$products[]=$tmp;
			} 	
		}
	}else{
		header('location:../mainjq.php');
			exit;
	}	
	?>
<html>
<head>
	<title><?=$market_name?></title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="../css/main.css"/>
<link rel="stylesheet" href="../jquerymobile/jquery.mobile-1.4.5.min.css"/>
<script src="../jquerymobile/jquery.js"></script>
<script src="../jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script  type="text/javascript">
	function add_to_cart(x){
			 $.ajax({
				type: 'GET',
				url: 'add_to_cart.php',
				data: {pid:x},
				dataType: 'json',
				success: function(data){
					console.log(data);
					if(data.success==true){
						var pid=data.pid;
						$('.ui-li-count').html(data.count);
						$('#'+pid+'').css('border-style', 'solid');
						$('#'+pid+'').css('border-color', 'YellowGreen');
					}
				}
			 });
	}
	$(document).ready(function(){

		 $('.gohome').click(function(){ 
			 window.location='../mainjq.php';
		 });
		 $('.cat').click(function(){ 
			 var mid=<?=$mid?>;
			 window.location='../categories.php?mid='+mid;
		 });
		 $('#cart').click(function(){ 
			 window.location='../cart/cart.php';
		 });
		 

	});

</script>
	<body>
		<div data-role='page' id='promotions_loader_page'>
			<div data-role='header' data-position='fixed'><!--Header-->
				<h2><?=$market_name?></h2>
					<div data-role='navbar' data-iconpos='left'><!--Navbar-->
					<ul>
					<li><a href='#' data-icon='home' class='gohome'>Home</a></li>
					<li><a href='#' data-icon='bullets' class='cat'>Categories</a></li>
						<?php				
						if(isset($_SESSION['login'])){
							$count=count($_SESSION['cart']);
							echo "<li><a href='#' data-icon='shop' id='cart' >Cart <span class='ui-li-count'>$count</span></a></li>";
						}
						?>
						
					</ul>
					</div><!--/Navbar-->
			</div><!--/Header-->
			<div data-role='main' class='ui-content'>
		
					
					<?php
						/*
						0->pid
						1->mid
						2->name
						3->catid
						4->price
						5->quantity
						6->unit
						7->start
						8->end
						9->im_dir
						*/
						
						$count=0;
						while($count<count($products)){
							echo "<div class='card' id={$products[$count][0]}>";
							echo "<h3>".$products[$count][2]."</h3>";
							echo "<div class='card'>"."<img src='data:image/jpeg;base64,".base64_encode($products[$count][9])."' height='40%' width='100%'/>"."</div><br> ";
							echo "<button data-icon='shop' class='ui-shadow ui-btn-inline  ui-btn-icon-left ui-corner-all' onClick='add_to_cart(".$products[$count][0].")'";
							if(!isset($_SESSION['login'])){
								echo "disabled";
							}
							echo ">"." ".$products[$count][5]." ".$products[$count][6]." for ".$products[$count][4]." L.L</button>";
							echo "</div>";
							$count++; 
						}	
			
						?>
					</div><!--Closure of main-->
		</div><!--Closure of the page-->
			
		</body>
		</html>
		