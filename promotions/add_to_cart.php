<?php
session_start();
if(!isset($_SESSION['login'])){
	header('location:../mainjq.php');
	exit;
}
require_once('../functions/database.php');
class CART{
	function add_product(){
		if(isset($_GET['pid'])){
		$return=array();
		extract($_GET);
		$return['success']=false;
		$link=connect();
		$userid=$_SESSION['userid'];
		$request1="SELECT mid,price FROM promotions WHERE pid=$pid;";
		$result1=mysqli_query($link,$request1);
		$tab1=mysqli_fetch_row($result1);
		$request="INSERT INTO shoppingcart (`cid`,`mid`,`pid`,`price`) VALUES ($userid,$tab1[0],$pid,$tab1[1]);";
		if($result=mysqli_query($link,$request)){
			$query="SELECT name,price FROM promotions WHERE pid=$pid;";
			$result2=mysqli_query($link,$query);
			$tab2=mysqli_fetch_row($result2);
			$_SESSION['cart'][]=$pid;
			$return['success']=true;
			$return['pid']=$pid;
			$return['count']=count($_SESSION['cart']);
			$return['msg']="<div style='padding:10px 20px;'><h2>Added To Cart</h3></div>";
		}
		return json_encode($return);
		}
	}
}
$obj=new CART;
echo $obj->add_product();