<?php
session_start();
require_once('functions/database.php');
$link=connect();
if(isset($_GET['mid'])){
	extract($_GET);
}else{
	header('location:mainjq.php');
	exit;
}
?>
<html>
<head>
<title>WEBOFFERS</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="jquerymobile/jquery.mobile-1.4.5.min.css" />
<script src="jquerymobile/jquery.js"></script>
<script src="jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script  type="text/javascript">
function gotocat(x){
	var mid=<?=$mid?>;
	var catid=x;
	window.location='promotions/promotions_loader.php?mid='+mid+'&catid='+x;
}
</script>
<?php
	$query='SELECT * FROM category;';
	$res=mysqli_query($link,$query);
	$tab=array();
	$cat=array();
	while($cat=mysqli_fetch_row($res)){
		$tab[$cat[0]]=[$cat[1],$cat[2]];
	}
	?>
	<div data-role='page' id='page-categories'>
		<div data-role='header'>
			<h1>Categories</h1>
		</div>
	 <div data-role='main' class='ui-content'>
		 <ul data-role='listview'>
			 <?php
				 foreach($tab as $catid=>$attr){
					 //Now get the number of elements from each category
					 if($catid==0){
						 $query_num="SELECT * FROM promotions WHERE mid=$mid";
						 }else{
							 $query_num="SELECT * FROM promotions WHERE mid=$mid AND catid=$catid;";
						 }			 
					 $res_num=mysqli_query($link,$query_num);
					 $num=mysqli_num_rows($res_num);
					 echo "<li><a href='#' class='ui-btn ui-shadow' onclick='gotocat($catid)' ><img src='{$tab[$catid][1]}' class='ui-li-icon ui-corner-none'>{$tab[$catid][0]} <span class='ui-li-count'>$num</span></a></li>";
				 }
				 ?>
				 </ul>
				 </div>
	</div>
	</body>
	</html>