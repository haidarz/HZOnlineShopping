<?php
session_start();
if(!isset($_SESSION['login'])){
      header('location:../mainjq.php');
      exit;
 }
require_once('../functions/database.php');
$link=connect();
?>
<html>
<head>
<title>My Cart</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="../css/main.css"/>
<link rel="stylesheet" href="../jquerymobile/jquery.mobile-1.4.5.min.css" />
<script src="../jquerymobile/jquery.js"></script>
<script src="../jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<!--Javascript-->
<script  type="text/javascript">
	function remove_promo(x){
		 $.ajax({
			type: 'GET',
			url: 'remove.php',
			data: {pid:x,cid:<?php echo $_SESSION['userid'];?>},
			dataType: 'json',
			success: function(data){
				console.log(data);
			}
		});
	}
$(document).ready(function(){
	
	$('#pay_btn').click(function(){
		$.ajax({
			type: 'GET',
			url: 'pay_processor.php',
			success: function(data){
				console.log(data);
			}
		});
	});
	
	

	$('.gohome').click(function(){ 
			 window.location='../mainjq.php';
		 });
		 
	$('.toremove').click(function() {
      $(this).parent('li').remove();
 	 });
	  
	  $('#chk_btn').click(function(){ 
			 window.location='question2.php';
		 });
	  
	
});
	
	
</script>
</head>
<body>
<div data-role="page" id="page_my_cart" data-theme='a' data-position='fixed'>
	<div data-role='header'>
		<h1>My Cart</h1>
		<div data-role='navbar' data-iconpos='left'>
			<ul>
				<li><a href='#' data-icon='home' class='gohome'>Home</a></li>
				<li><a href='#' data-icon='action' class='share'>Share cart</a></li>
			</ul>
		</div>
	</div>
	
	<div data-role='main' class='ui-content'>
	<?php	
		if(isset($_SESSION['cart'])){
	         $cart=$_SESSION['cart'];
			 $my_cart=array();
	         $link=mysqli_connect("localhost","root","","project_db");
	         foreach($cart as $pid){
		     $request="SELECT name,price FROM promotions WHERE pid=$pid ;";
		     $result=mysqli_query($link,$request);
		     $tab=mysqli_fetch_row($result);
		     $my_cart[$pid]=array($tab[0],$tab[1]);
			 
	     }
		 
	
	?>
	<ul data-role='listview' id='promo_viewer' data-split-icon='delete' data-split-theme='a' data-inset='true'>
					<?php
						$sum=0;
						foreach($my_cart as $p_id=>$tmp){
						$sum=$sum+$tmp[1];
						echo "<li id='$p_id"."li'><a href='#'>$tmp[0] ||  $tmp[1] L.L</a><a href='#' class='toremove' onclick='remove_promo($p_id)'></a></li>";
						}
		}
		
						?>
				</ul>
<?php if(count($cart)==0){
				 echo "<h2 align='center'>Your cart is empty</h2>";
			 }
		if($sum>0){
			echo "<a href='#' class='ui-btn ui-shadow ui-corner-all ui-icon-shop ui-btn-icon-left ui-btn-a' id='pay_btn'>Pay ||  $sum  L.L </a>";
		}
		?>
		
		<a href='#' class='ui-btn ui-shadow ui-corner-all ui-icon-check ui-btn-icon-left ui-btn-a' id='chk_btn'>Check</a>
		</div><!--End of main-->
	</div><!--End of page-->
</body>
</html>