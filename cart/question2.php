<?php
session_start();
require_once('../functions/database.php');
$link=connect();
?>
<html>
<head>
<title>HZ Shopping Site</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="../jquerymobile/jquery.mobile-1.4.5.min.css" />
<script src="../jquerymobile/jquery.js"></script>
<script src="../jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script>
	$(document).ready(function(){
		
		$('#check_date').click(function(){
			date_start=document.getElementById('start_date_q2').value;
			date_end=document.getElementById('end_date_q2').value;
		$.ajax({
				type: 'GET',
				url: 'question2_processor.php',
				data: {date1:date_start,date2:date_end},
				success: function (data) {
					console.log(data);
					$('#result_check').html(data);
				}	
		});
	
	});

	});
	

</script>
</head>
<body>
<div data-role='page' id='page_new'>
<div data-role='header'>
	<h1>Question 2</h1>
</div>
<div data-role='main' class='ui-content'>
	<input type='date' name='start_date' id='start_date_q2' placeholder='Starting date'>
	<input type='date' name='end_date' id='end_date_q2' placeholder='Ending date'>
	<a href='#' class='ui-btn ui-shadow ui-corner-all ui-icon-check ui-btn-icon-left ui-btn-a' id='check_date'>Check</a>
	<div id='result_check'></div>
</div>
</div>
</body>
</html>