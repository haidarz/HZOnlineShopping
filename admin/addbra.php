<?php
	session_start();
	if(!isset($_SESSION['admin'])){
		header('location:../mainjq.php');
		exit;
	}
require_once('../functions/database.php');
$link=connect();
?>
<html>
<head>
<title>Admin Panel</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="../jquerymobile/jquery.mobile-1.4.5.min.css"/>
<script src="../jquerymobile/jquery.js"></script>
<script src="../jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script>
$(document).ready(function(){
		//Submitting Form 
        $('#form_add_branch').submit(function(event) {
                event.preventDefault();
                $.ajax({
                        type: 'POST',
                        url: 'addbra_processor.php',
                        data: $(this).serialize(),
                        dataType: 'json',
                        success: function (data) {
                                console.log(data);
								
						}
				});
		});
		//End of form submission
		
		$('.gohome').click(function(){ 
			 window.location='../mainjq.php';
		 });
		$('.goAdmin').click(function(){ 
			 window.location='./Admin.php';
		 });

});
</script>
<body>
	<div data-role='page' id='page_add_branch'>
<div data-role='header'>
		<h2>Admin Panel</h2>
		<div data-role='navbar' data-iconpos='left'>
			<ul>
				<li><a href='#' data-icon='home' class='gohome'>Home</a></li>
				<li><a href='#' data-icon='gear' class='goAdmin'>Admin Panel</a></li>
			</ul>
		</div>
	</div>		
<form id='form_add_branch'>
	<!--Select Market-->
	<div class="ui-field-contain">
	<select id='select-market-list' name='mid' data-native-menu="false" required>
		<option disabled selected>Select Market</option>
		<?php
			$req="SELECT * FROM markets ;";
			$res=mysqli_query($link,$req);
			while($tab1=mysqli_fetch_row($res)){		
				echo "<option value=$tab1[0]>$tab1[1]</option>";
			}
		?>
	</select>
	</div>
	<!--/Select Market-->
	<input type='text' name='address' placeholder='Address' required>
	<input type='number' name='lat' placeholder='Latitude' step='any' min='-90' max='90' required>
	<input type='number' name='lng' placeholder='Longitude' step='any' min='-180' max='180' required>
	<input type='submit' value='Add Branch'>
</form>

</div><!--Closure of the page-->

</body>
</html>