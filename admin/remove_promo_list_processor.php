<?php
	session_start();
	if(!isset($_SESSION['admin'])){
		header('location:../mainjq.php');
		exit;
	}
	?>
<html>
<head>
<title>WEBOFFERS</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="../jquerymobile/jquery.mobile-1.4.5.min.css" />
<script src="../jquerymobile/jquery.js"></script>
<script src="../jquerymobile/jquery.mobile-1.4.5.min.js"></script>
</head>
<body>
<?php
	require_once('../functions/database.php');
			if(isset($_POST['market'])){
				extract($_POST);
				$link=connect();
				$query="SELECT pid,name FROM promotions WHERE mid=$market;";
				$result=mysqli_query($link,$query);
				$tab=array();
				if(mysqli_num_rows($result)==0){
					$str='<option selected disabled>No Promotions available</option>';
				}else{
					$str="<option disabled selected>Select a promotion</option>";
					while($tab=mysqli_fetch_row($result)){
						$str.="<option value=$tab[0]>$tab[1]</option>";
					}
				}
			}else{
				$str='<option>Error in connection</option>';
			}
	?>
	<div data-role='page' id='pagedeleteloader'>
		<div data-role='main' class='ui-content'>
			<div class='ui-select'  id='select_promo_to_delete_div'>
	<div class='ui-field-contain'>
	<select id='select_promo_to_delete' name='select_promo_to_delete' data-native-menu='false' required>
			<?=$str?>
				</select>
	</div>
	</div>
	</div>
	</div>
</body>