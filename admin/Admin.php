<?php
	session_start();
	if(!isset($_SESSION['admin'])){
		header('location:../mainjq.php');
		exit;
	}
?>
<html>
<head>
<title>Admin Panel</title>
<!--Mobile Webpage Properties-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--Requiring all needed libraries-->
<link rel="stylesheet" href="../jquerymobile/jquery.mobile-1.4.5.min.css"/>
<script src="../jquerymobile/jquery.js"></script>
<script src="../jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script>
	
	$(document).ready(function(){	
		
	$('#href_addpro').click(function(){
		window.location='pg-admin-add-promotion.php';
		});
	$('#href_deletepro').click(function(){
		window.location='pg-admin-rm-promotion.php';
		});	
	$('#href_addbra').click(function(){
		window.location='addbra.php';
		});
	$('#href_rembra').click(function(){
		window.location='pg_admin_remove_branch.php';
		});
		
		
	$('.gohome').click(function(){ 
			 window.location='../mainjq.php';
		 });
		
	});
	
</script>
</head>
<body>
<div data-role="page" id="admin_page">
	<div data-role='header'>
		<h2>Admin Panel</h2>
		<div data-role='navbar' data-iconpos='left'>
			<ul>
				<li><a href='#' data-icon='home' class='gohome'>Home</a></li>
			</ul>
		</div>
	</div>
<div data-role="main" class='ui-content'>
<a href='#' id='href_addpro' class='ui-btn ui-shadow'>Add Promotion</a>
<a href='#' id="href_deletepro" class='ui-btn ui-shadow'>Remove Promotion</a>
<a href="#" id='href_addbra' class='ui-btn ui-shadow'>Add Branch</a>
<a href="#" id='href_rembra' class='ui-btn ui-shadow'>Remove Branch</a> 
</div>
</div>
</body>
</html>