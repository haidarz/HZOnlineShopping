
<?php
session_start();
class SIGNUPVALIDATOR{
	function signup_check(){
		//Preparing regex formulas
		$regex_user="/\w{3,18}/";
		$regex_email="/^\w([-_.]?\w)*@\w([-_.]?\w)*\.(([a-z]){2,4})$/";
		$regex_name="/([a-z]){3,12}/i";
		$regex_password="/.{6,20}/";
		$regex_tel="/([0-9]){8}/";
		$return=array();
		$return['success']=false;
		$return['msg']='';
		if(!empty($_POST['username']) && !empty($_POST['password']) &&!empty($_POST['email']) && !empty($_POST['first']) && !empty($_POST['last']) && !empty($_POST['telephone']) ){
				extract($_POST);
				if( (!preg_match($regex_user,$username)) || (!preg_match($regex_email,$email)) || (!preg_match($regex_name,$first)) || (!preg_match($regex_name,$last)) || (!preg_match($regex_tel,$telephone)) || (!preg_match($regex_password,$password)) ){
					$return['success']=false;
					$return['msg']='<h3>Invalid input !</h3>';
					return json_encode($return);
				}else{
				
				require_once('database.php');
				$link=connect();
				$req="SELECT username from clients WHERE username='$username';";
				$res=mysqli_query($link,$req);
				if(mysqli_num_rows($res)==0){
					$request="INSERT INTO clients (username,password,email,firstname,lastname,tel) values('$username','$password','$email','$first','$last','$telephone');";
					$result=mysqli_query($link,$request);
					$return['success']=true;
					$return['msg']='<div style="padding:10px 20px;"><h2>Success!</h2></div>';
					return json_encode($return);
				}else{
					$return['success']=false;
					$return['msg']='<div style="padding:10px 20px;"><h2>Username already exists!</h2></div>';
					return json_encode($return);
				}
				}
		}else{
			$return['success']=false;
			$return['msg']='<div style="padding:10px 20px;"><h2>We need more data!</h2></div>';
			return json_encode($return);
		}
	}
}
$SIGNUPVALIDATOR = new SIGNUPVALIDATOR;
echo $SIGNUPVALIDATOR->signup_check();
?>