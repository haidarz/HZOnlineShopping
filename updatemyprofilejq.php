<?php
session_start();
if(!isset($_SESSION['login'])){
	header('location:mainjq.php');
	exit;
}
?>
<html>
<head>
<title>WEBOFFERS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="jquerymobile/jquery.mobile-1.4.5.min.css" />
<script src="jquerymobile/jquery.js"></script>
<script src="jquerymobile/jquery.mobile-1.4.5.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
</head>
<script>
//My script goes here
	
$(document).ready(function(){
	
	$.ajax({			
                        type: 'POST',
                        url: 'viewmyprofilevalidation.php',
                        dataType: 'json',
                        success: function (data) {
                                console.log(data);
								var a=document.getElementsByClassName('viewus');
								var b=document.getElementsByClassName('viewfirst');	
								var c=document.getElementsByClassName('viewlast');	
								var d=document.getElementsByClassName('viewemail');
								var e=document.getElementsByClassName('viewtel');
								var f=document.getElementsByClassName('viewpass');
								a[0].value=data.username;
								b[0].value=data.firstname;
								c[0].value=data.lastname;
								d[0].value=data.email;
								e[0].value=data.tel;
								f[0].value=data.password;
						}
	    	});
	$('#update-form').click(function(event) {
                event.preventDefault();
				var a=document.getElementById('viewemail1').value;
				var b=document.getElementById('viewtel1').value;
				var c=document.getElementById('viewpass1').value;
				var d=document.getElementById('viewus1').value;
                $.ajax({
                        type: 'POST',
                        url: 'updatemyprofilevalidation.php',
                        data: {username:d,email:a,tel:b,password:c},
                        dataType: 'json',
                        success: function (data) {
                                console.log(data);
								$('#popup_msg').html(data.msg);
								$('#popup_msg').popup('open');
						}
				});
		});	
});
</script>
<body>
<div data-role='page' id='page-updatemyprofile'>
<div data-role='header' data-add-back-btn='true'>
<h1>My Profile</h1>
</div>

<div data-role='main' class='ui-content'>
							<form id='updateprofform'>
							<label for='viewus1'>Username</label>
							<input type='text' name='username' id='viewus1' class='viewus' disabled>
							<label for='viewfirst1'>First name</label>
							<input type='text' name='firstname'  id='viewfirst1' class='viewfirst' disabled>
							<label for='viewlast1'>Last name</label>
							<input type='text' name='lastname'  id='viewlast1' class='viewlast' disabled>
							<label for='viewemail1'>Email</label>
							<input type='email' name='email'  id='viewemail1' class='viewemail'>
							<label for='viewtel1'>Telephone</label>
							<input type='tel' name='tel' id='viewtel1' class='viewtel'>
							<label for='viewpass1'>Password</label>
							<input type='password' name='password'  id='viewpass1' class='viewpass'>
							<a href='#popup_msg' data-rel='popup' data-position-to='window' class='ui-btn ui-shadow ui-btn-a' data-transition='pop' id='update-form'>Update</a>
							</form>
</div>
<!--Message-->
<div data-role='popup' id='popup_msg' data-theme="a" class="ui-corner-all">
<div style='padding:10px 20px;' id='update_msg'>
<h3>Message</h3>
</div>
</div>
<!--End of Message-->

</div>

</body>
</html>