<?php
session_start();
if(!isset($_SESSION['login'])){
	header('location:mainjq.php');
	exit;
}
class VIEW_MY_PROFILE{
	function view_my_profile_f(){
		$return=array();
		require_once('functions/database.php');
		$link=connect();
		$userid=$_SESSION['userid'];
		$query="SELECT username,firstname,lastname,email,tel,password FROM clients WHERE cid=$userid;";
		$res=mysqli_query($link,$query);
		$table=mysqli_fetch_row($res);
		$return['username']=$table[0];
		$return['firstname']=$table[1];
		$return['lastname']=$table[2];
		$return['email']=$table[3];
		$return['tel']=$table[4];
		$return['password']=$table[5];
		return json_encode($return);
	}
}
$VIEW_MY_PROFILE=new VIEW_MY_PROFILE;
echo $VIEW_MY_PROFILE->view_my_profile_f();
